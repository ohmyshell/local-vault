(function () {

	const { ipcRenderer } = require('electron');

	document.querySelector('#close').onclick = sendClose;
	document.querySelector('#min').onclick = sendMinimize;
	document.querySelector('#create').onclick = createDialog;
	document.querySelector('#remove').onclick = removeDialog;
	document.querySelector('#get').onclick = getDialog;
	document.querySelector('#generate').onclick = generateDialog;
	document.querySelector('#change').onclick = changeDialog;
	document.querySelector('#showall').onclick = showallDialog;
	document.querySelector('#delete').onclick = deleteDialog;

	ipcRenderer.on('content-reply', (_, msg) => {
		if (msg)
			document.querySelector('#log').innerHTML = msg;
	});

	ipcRenderer.on('showall-passwords-reply', (_, msg) => {
		if (msg) {
			hideAllContent();
			document.querySelector('#passwords-preview').hidden = false;
			document.querySelector('#passwords-preview').innerHTML = "";
			Object.keys(msg).forEach(entry => {
				let title = document.createElement('h4');
				title.innerHTML = entry;
				let table = document.createElement('table');
				table.className = 'table table-secondary table-striped';
				let thead = document.createElement('thead');
				thead.className = 'table thead-dark';
				let thu = document.createElement('th');
				thu.innerHTML = 'Username';
				let thp = document.createElement('th');
				thp.innerHTML = 'Password';
				thead.appendChild(thu);
				thead.appendChild(thp);
				let tbody = document.createElement('tbody');
				msg[entry].forEach(user => {
					let tr = document.createElement('tr');
					let tdu = document.createElement('td');
					let tdp = document.createElement('td');
					tdu.innerHTML = Object.keys(user)[0];
					tdp.innerHTML = user[tdu.innerHTML];
					tr.appendChild(tdu);
					tr.appendChild(tdp);
					tbody.appendChild(tr);
				});
				table.appendChild(thead);
				table.appendChild(tbody);
				document.querySelector('#passwords-preview').appendChild(title);
				document.querySelector('#passwords-preview').appendChild(table);
			})
			document.querySelector('#passwords-preview').hidden = false;
		}
	});

	ipcRenderer.on('passwords-reply', (_, msg) => {
		if (msg) {
			hideAllContent();
			document.querySelector('#passwords-preview').hidden = false;
			let table = document.createElement('table');
			let thead = document.createElement('thead');
			table.className = 'table table-secondary table-striped';
			thead.className = 'table thead-dark';
			let thu = document.createElement('th');
			thu.innerHTML = 'Username';
			let thp = document.createElement('th');
			thp.innerHTML = 'Password';
			thead.appendChild(thu);
			thead.appendChild(thp);
			let tbody = document.createElement('tbody');
			msg.forEach(entry => {
				let tr = document.createElement('tr');
				let tdu = document.createElement('td');
				let tdp = document.createElement('td');
				tdu.innerHTML = Object.keys(entry)[0];
				tdp.innerHTML = entry[tdu.innerHTML];
				tr.appendChild(tdu);
				tr.appendChild(tdp);
				tbody.appendChild(tr);
			})
			table.appendChild(tbody);
			table.appendChild(thead);
			document.querySelector('#passwords-preview').innerHTML = "";
			document.querySelector('#passwords-preview').appendChild(table);
			document.querySelector('#passwords-preview').hidden = false;
		}
	});

	function create(e) {
		e.preventDefault();
		let password = document.querySelector('#create-content > form input').value;
		ipcRenderer.send('action', ['create', password]);
	}

	function remove(e) {
		e.preventDefault();
		let args = ['remove'];
		document.querySelectorAll('#remove-content > form input').forEach(field => {
			args.push(field.value);
		});
		ipcRenderer.send('action', args);
	}

	function get(e) {
		e.preventDefault();
		let args = ['get'];
		document.querySelectorAll('#get-content > form input').forEach(field => {
			args.push(field.value);
		});
		ipcRenderer.send('action', args);
	}

	function generate(e) {
		e.preventDefault();
		let args = ['generate'];
		document.querySelectorAll('#generate-content > form input').forEach(field => {
			args.push(field.value);
		});
		ipcRenderer.send('action', args);
	}

	function change(e) {
		e.preventDefault();
		let args = ['change'];
		document.querySelectorAll('#change-content > form input').forEach(field => {
			args.push(field.value);
		});
		ipcRenderer.send('action', args);
	}

	function showall(e) {
		e.preventDefault();
		let args = ['showall'];
		args.push(document.querySelector('#showall-content > form input').value);
		ipcRenderer.send('action', args);
	}

	function deleteEnc(e) {
		e.preventDefault();
		let args = ['delete'];
		document.querySelectorAll('#delete-content > form input').forEach(field => {
			args.push(field.value);
		});
		ipcRenderer.send('action', args);
	}

	function sendMinimize() {
		ipcRenderer.send('command', 'min');
	}

	function sendClose() {
		ipcRenderer.send('command', 'close');
	}

	function hideAllContent() {
		document.querySelector('#passwords-preview').innerHTML = "";
		document.querySelectorAll('#content input').forEach(entry=>entry.value="");
		document.querySelector('#content').childNodes.forEach(child => {
			child.hidden = true;
		})
	}

	function createDialog() {
		hideAllContent();
		document.querySelector('#create-content').hidden = false;
		document.querySelector('#create-content > form > button').onclick = create;
	}

	function removeDialog() {
		hideAllContent();
		document.querySelector('#remove-content').hidden = false;
		document.querySelector('#remove-content > form > button').onclick = remove;
	}

	function getDialog() {
		hideAllContent();
		document.querySelector('#get-content').hidden = false;
		document.querySelector('#get-content > form > button').onclick = get;
	}

	function generateRandom(e) {
		e.preventDefault();
		document.querySelectorAll('#generate-content > form input')[2].value = generateRandomPass();
	}

	function generateDialog() {
		hideAllContent();
		document.querySelector('#generate-content').hidden = false;
		document.querySelectorAll('#generate-content > form button')[0].onclick = generateRandom;
		document.querySelectorAll('#generate-content > form button')[1].onclick = generate;
	}

	function changeDialog() {
		hideAllContent();
		document.querySelector('#change-content').hidden = false;
		document.querySelector('#change-content > form > button').onclick = change;
	}

	function showallDialog() {
		hideAllContent();
		document.querySelector('#showall-content').hidden = false;
		document.querySelector('#showall-content > form > button').onclick = showall;
	}

	function deleteDialog() {
		hideAllContent();
		document.querySelector('#delete-content').hidden = false;
		document.querySelector('#delete-content > form > button').onclick = deleteEnc;
	}

	function generateRandomPass(i = 12, pass = "") {
		pass += randomChar();
		if (i > 1) return generateRandomPass(--i, pass);
		else return pass;
	}

	function randomChar() {
		let random = Math.random() * 100;
		if (random <= 40) return String.fromCharCode(Math.floor(Math.random() * 26) + 97);
		else if (random > 40 && random <= 80) return String.fromCharCode(Math.floor(Math.random() * 26) + 65);
		else if (random > 80 && random <= 90) return Math.round(Math.random() * 9);
		else if (random > 90 && random <= 95) return String.fromCharCode(95);
		else return String.fromCharCode(35);
	}
})();