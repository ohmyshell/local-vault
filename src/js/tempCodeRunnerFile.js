function generateRandomPass(i = 12, pass = "") {
	pass += randomChar();
	if (i > 1) return generateRandomPass(--i, pass);
	else return pass;
}

function randomChar() {
	let random = Math.random() * 100;
	if (random <= 20) return String.fromCharCode(Math.floor(Math.random() * 26) + 97);
	else if (random > 20 && random <= 40) return String.fromCharCode(Math.floor(Math.random() * 26) + 65);
	else if (random > 40 && random <= 60) return Math.round(Math.random() * 9);
	else if (random > 60 && random <= 80) return String.fromCharCode(95);
	else return String.fromCharCode(35);
}

console.log(generateRandomPass());