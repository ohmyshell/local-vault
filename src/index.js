const { app, BrowserWindow, ipcMain, Menu } = require('electron');
const fs = require('fs');
const crypto = require('crypto');

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require('electron-squirrel-startup')) { // eslint-disable-line global-require
  app.quit();
}

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;

const createWindow = () => {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    minWidth: 1000,
    minHeight: 600,
    frame: false,
    resizable: true,
    title: 'Local Vault',
    icon: './src/icon/icon.icns'
  });

  var template = [{
    label: "Application",
    submenu: [
      { label: "About Application", selector: "orderFrontStandardAboutPanel:" },
      { type: "separator" },
      { label: "Quit", accelerator: "Command+Q", click: function () { app.quit(); } }
    ]
  }, {
    label: "Edit",
    submenu: [
      { label: "Undo", accelerator: "CmdOrCtrl+Z", selector: "undo:" },
      { label: "Redo", accelerator: "Shift+CmdOrCtrl+Z", selector: "redo:" },
      { type: "separator" },
      { label: "Cut", accelerator: "CmdOrCtrl+X", selector: "cut:" },
      { label: "Copy", accelerator: "CmdOrCtrl+C", selector: "copy:" },
      { label: "Paste", accelerator: "CmdOrCtrl+V", selector: "paste:" },
      { label: "Select All", accelerator: "CmdOrCtrl+A", selector: "selectAll:" }
    ]
  }
  ];

  Menu.setApplicationMenu(Menu.buildFromTemplate(template));

  // and load the index.html of the app.
  mainWindow.loadURL(`file://${__dirname}/index.html`);

  // Open the DevTools.
  // mainWindow.webContents.openDevTools();

  // Emitted when the window is closed.
  mainWindow.on('closed', () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
  });
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow();
  }
});

process.on('uncaughtException', function (exception) {
  // handle or ignore error
  console.log(exception);

});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and import them here.

let dir = app.getPath('userData') + '\\data.enc';
ipcMain.on('action', (event, args) => {
  if (args[0] === 'create') {
    if (exists(dir)) {
      event.sender.send('content-reply', 'File already exists, Didn\'t Overwrite');
    } else {
      let json = {};
      fs.writeFileSync(dir, encrypt(args[1], JSON.stringify(json)));
      event.sender.send('content-reply', 'File created at: ' + dir);
    }
  }
  else if (args[0] === 'generate') {
    if (exists(dir)) {
      let text = fs.readFileSync(dir, 'utf8');
      let json;
      try {
        json = JSON.parse(decrypt(args[4], text));
      } catch {
        event.sender.send('content-reply', 'Wrong Master Password');
      }
      if (!json[args[2]]) json[args[2]] = [];
      let exists = false;
      json[args[2]].forEach(element => {
        if (Object.keys(element)[0] === args[1]) {
          exists = true;
        }
      })
      if (exists) {
        event.sender.send('content-reply', 'Username already exists, Delete username first');
      } else {
        json[args[2]].push({ [args[1]]: args[3] });
        fs.writeFileSync(dir, encrypt(args[4], JSON.stringify(json)));
        event.sender.send('content-reply', 'Password Added.');
      }
    } else event.sender.send('content-reply', 'File not found, Create file');

  }
  else if (args[0] === 'change') {
    let text = fs.readFileSync(dir, 'utf8');
    let json;
    try {
      json = JSON.parse(decrypt(args[1], text));
    } catch {
      event.sender.send('content-reply', 'Wrong Master password');
    }
    if (args[2] === args[3]) {
      fs.writeFileSync(dir, encrypt(args[2], JSON.stringify(json)));
      event.sender.send('content-reply', 'Password Changed');
    } else event.sender.send('content-reply', 'Wrong Re-typed password');
  }
  else if (args[0] === 'get') {
    if (exists(dir)) {
      let text = fs.readFileSync(dir, 'utf8');
      let json;
      try {
        json = JSON.parse(decrypt(args[2], text));
      } catch (error) {
        event.sender.send('content-reply', 'Wrong Master Password');
      }
      if (!json[args[1]]) {
        event.sender.send('content-reply', 'Website not found');
        return false;
      }
      event.sender.send('passwords-reply', json[args[1]]);
    } else event.sender.send('content-reply', 'File not found, Create file');
  }
  else if (args[0] === 'remove') {
    if (exists(dir)) {
      let text = fs.readFileSync(dir, 'utf8');
      let json;
      try {
        json = JSON.parse(decrypt(args[3], text));
      } catch (error) {
        event.sender.send('content-reply', 'Wrong Master Password');
      }
      if (!json[args[2]]) {
        event.sender.send('content-reply', 'Username/Website not found');
        return false;
      }
      json[args[2]].forEach((element, index) => {
        if (Object.keys(element)[0] === args[1]) {
          json[args[2]].splice(index, 1);
        }
      });
      if (json[args[2]].length === 0) delete json[args[2]];
      fs.writeFileSync(dir, encrypt(args[3], JSON.stringify(json)));
      event.sender.send('content-reply', 'Password Removed');
    } else event.sender.send('content-reply', 'File not found, Create file');
  }
  else if (args[0] === 'showall') {
    if (exists(dir)) {
      let text = fs.readFileSync(dir, 'utf8');
      let json;
      try {
        json = JSON.parse(decrypt(args[1], text));
      } catch (error) {
        event.sender.send('content-reply', 'Wrong Master Password');
      }
      event.sender.send('showall-passwords-reply', json);
    } else event.sender.send('content-reply', 'File not found, Create file');
  }
  else if (args[0] === 'delete') {
    if (!exists(dir)) {
      event.sender.send('content-reply', 'File doesn\'t exist');
    } else {
      if (args[1] !== args[2]) event.sender.send('content-reply', 'Master Password doesn\'t match');
      else {
        try {
          let text = fs.readFileSync(dir, 'utf8');
          let test = JSON.parse(decrypt(args[1], text));
          fs.unlinkSync(dir);
          event.sender.send('content-reply', 'File deleted');
        } catch (error) {
          event.sender.send('content-reply', 'Wrong Master Password');
        }
      }
    }
  }
});

ipcMain.on('command', (event, args) => {
  if (args === "min") {
    mainWindow.minimize();
  }
  else if (args === "close") {
    app.quit();
    process.exit(0);
  }
});

//---- Helper Methods ----//
function encrypt(master, text) {
  let algorithm = 'aes-256-cbc';
  let key = crypto.createCipher(algorithm, master);
  let enc = key.update(text, 'utf8', 'hex');
  enc += key.final('hex')
  return enc;
}

function decrypt(master, text) {
  let algorithm = 'aes-256-cbc';
  let key = crypto.createDecipher(algorithm, master);
  let dec = key.update(text, 'hex', 'utf8');
  dec += key.final('utf8');
  return dec;
}

function exists(dir) {
  try {
    return fs.statSync(dir);
  } catch (e) {
    return false;
  }
}